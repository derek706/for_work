//左側拉出購物列表
$(document).ready(function() {
	$('[id="left-menu"]').each(function(idx,obj){
		$(obj).sidr({
		  name: 'sidr-left',
		  side: 'left' // By default
		});
	});

    $('#left-menu2').sidr({
      name: 'sidr-left',
      side: 'left' // By default
    });

    $('#right-menu').sidr({
      name: 'sidr-right',
      side: 'right'
    });
});

//購物列表收合
$(document).ready(function(){
	$(".shopping-list-wrapper").hide();
	$(".shopping-list-more").click(function(){
		$(".shopping-list-wrapper").slideToggle("20");
		$(this).css("transform","180deg");
	});
});

//購物車明細頁-規格
$(function(){
	$(".specs li").click(function(){
		  $(".specs li").removeClass("current");
      $(this).addClass("current");
    });
});


//購買數量增減模組
	$('.qtyInputBox .input-group-append').click(function(){
		var qty = $('.qtyInputBox input').val();
		qty = parseInt(qty) + 1;
		$('.qtyInputBox input').val(qty);
	});
	$('.qtyInputBox .input-group-prepend').click(function(){
		var qty = $('.qtyInputBox input').val();
		if (qty<2){
			$('.qtyInputBox input').val(1);
		} else {
			qty = qty - 1;
			$('.qtyInputBox input').val(qty);
		}
	});
