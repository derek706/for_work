// JavaScript Document

// 手機menu
$(document).ready(function(){
	$('#menu').slicknav();
});

// 錨點滑動 
	$(function() {
		$('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html, body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});


  
// top 
	$(function () {
		$.scrollUp({
			scrollName: 'scrollUp', // Element ID
			topDistance: '300', // Distance from top before showing element (px)
			topSpeed: 300, // Speed back to top (ms)
			animation: 'fade', // Fade, slide, none
			animationInSpeed: 200, // Animation in speed (ms)
			animationOutSpeed: 200, // Animation out speed (ms)
			scrollText: '', // Text for element
			activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
		});
	});



// ScrollReveal
window.sr = ScrollReveal({
					reset: true,
					scale: 0.7,
	        mobile: false
					/*
					// 參數設定說明
					origin: "bottom",  // 起始位置
					distance: "20px",  // 距離
					duration: 500,  // 動畫時間
					delay: 0,  // 動畫延遲時間
					rotate: { x: 0, y: 0, z: 0 },  // 旋轉角度
					opacity: 0,  // 透明度
					scale: 0.9, // 縮放比例
					easing: "cubic-bezier(0.6, 0.2, 0.1, 1)", // 動畫速度曲線
					container: window.document.documentElement, // 外層
					mobile: true, // 支援行動裝置
					reset: true, // 每次都啟動動畫
					useDelay: "always", // 延遲動畫次數
					viewFactor: 0.2, // 當該物件在可視範圍內，則顯示此物件(0.2表示可視範圍20%)
					viewOffset: { top: 0, right: 0, bottom: 0, left: 0 }, // 當外層有設定間隔造成偏移，則請設定在此維持精準度
					beforeReveal: function (domEl) { console.log(1) }, // 當啟動顯示前，則執行此函式
					beforeReset: function (domEl) {console.log(2) }, // 當重啟前，則執行此函式
					afterReveal: function (domEl) {console.log(3) }, // 當啟動後，則執行此函式
					afterReset: function (domEl) {console.log(4) } // 當重啟後，則執行此函式 
					*/
	
				});
        sr.reveal('.i-about-information>h2', {
					duration: 1000,
					distance: '100px',
          origin:'top'
        });
        sr.reveal('.i-about-contact', {
          duration: 1000,
					distance:'300px',
					rotate: { x: 0, y: 0, z: 360 },
					origin:'right',
        });
        sr.reveal('.i-news>h2', {
          duration: 1000,
					distance: '100px',
          origin:'top'
        });
        sr.reveal('.i-news>ul', {
          duration: 500,
          origin:'left',
        });
				sr.reveal('.i-news>ul>li', { delay:500, duration:1000 }, 200);
        sr.reveal('.i-product>h2', {
          duration: 1000,
					distance: '100px',
          origin:'top'
        });
        sr.reveal('.i-product .owl-carousel', {
          duration: 2000,
          origin:'bottom',
        });
        sr.reveal('.i-contact-information>h2', {
          duration: 1000,
					distance: '100px',
          origin:'bottom'
        });
				sr.reveal('.form-box>div', { delay:900, duration:1000 }, 200);
