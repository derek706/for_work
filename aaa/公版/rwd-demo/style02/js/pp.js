/*
*already include jQuery lib...
*/
//make a preivate scope of javascript
(function($,window){
  //do my script when document ready...
  $(document).ready(function(){
      //now we write our real code here...
    $("#myMenu").on("mousedown",function(e){
        var $tg=$(e.target);
        var tgBoxNum = $tg.attr("tgbox");
        //make all box hide
        $(".box").hide(); 
        //make selected box to show
        var $boxToShow =$(".box.b"+tgBoxNum);
        $boxToShow.fadeIn();
    });
    
  });
  
})(jQuery,window);