// JavaScript Document
 $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
				  400: {
                    items: 2
                  },
				  600: {
                    items: 3
                  },
                  900: {
                    items: 4
                  },
                  1100: {
                    items: 5
                  }
                }
              })
            })