// JavaScript Document
// 主選單按鈕
$(document).ready(function(){
	  $(".hamburger").click(function(){
		$(this).toggleClass("is-active");
	  });
	});

//主選單按鈕展開
$(document).ready(function () {
	$(".hamburger").click(function () {
		$("nav").toggle('slow');
	});
});

//搜尋點選展開
$(document).ready(function () {
	$(".search-icon").click(function () {
		$(".search-content").toggle('slow');
	});
});

//top
$(document).ready(function () {
	$.scrollUp({
		scrollName: 'scrollUp', // Element ID
		topDistance: '300', // Distance from top before showing element (px)
		topSpeed: 300, // Speed back to top (ms)
		animation: 'fade', // Fade, slide, none
		animationInSpeed: 200, // Animation in speed (ms)
		animationOutSpeed: 200, // Animation out speed (ms)
		scrollText: 'TOP', // Text for element
		activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	});
});


//分類展開
/* 選單點擊時焦點屬性自動添加 */
$(window).on("load", function () {
	"use strict";
	var j_setting = {
		tag:'products',					//-- *命名空間 不可重複使用
      	main_body: '.in-left-menu>ul, .in-left-menu>.m_classLink>ul',//--目標主體第一層大框架
		item:'.in-left-menu>ul>li, .in-left-menu>.m_classLink>ul>li',	//-- *第一層目標物件群 EX: '.prolist ul>li'
		resetitem:'#menu li:eq(1)', //-- *重置物件 (通常放到上板選單)
		addclass:'active',			//-- *增加的class屬性
		default:0,					//-- *預設第幾個為選取
		action:{main:'menu1',menu_child_body:'ul',menu_child_list:'li'}			//-- 模式 類型  menu1收合式選單
	};
	
	function work(j_setting){
		sessionStorage[j_setting.tag] = (typeof(sessionStorage[j_setting.tag])!=="undefined" ? sessionStorage[j_setting.tag]:j_setting.default);
		$(j_setting.item).each(function(idx,obj){
			$(obj).attr('jtagid',idx);
		}).bind('click',function(event){
			sessionStorage[j_setting.tag] = $(event.target).attr('jtagid');
		});
		$(j_setting.resetitem).bind('click',function(){sessionStorage[j_setting.tag]=j_setting.default;});
		$(j_setting.main_body).find(j_setting.action.menu_child_list+'[jtagid="'+sessionStorage[j_setting.tag]+'"]').parentsUntil(j_setting.main_body).each(function(idx,obj){
			if ($(obj).attr('jtagid')!==null && $(obj).attr('jtagid')!==''){
				$(obj).addClass(j_setting.addclass);
			}
		});
		if (typeof(j_setting.action)!=="undefined") {menu(j_setting);}
	}
	
	var section_back = function (e){
		var check_child = false;
		sessionStorage[j_setting.tag] = ($(e.target).attr('jtagid') ? $(e.target).attr('jtagid'):$(e.target).parentsUntil(j_setting.action.menu_child_body).last().attr('jtagid'));
      
     var 	notwork = $(e.target).parentsUntil(j_setting.main_body).last();
		$(j_setting.item).not(notwork).find(j_setting.action.menu_child_body).filter(':visible').slideUp();
      
		$(e.target).parentsUntil(j_setting.action.menu_child_body).each(function(idx,obj){
			if ($(obj).find(j_setting.action.menu_child_body).filter(':visible').length<=0 && $(obj).find(j_setting.action.menu_child_body).length>0 && $(obj).find(j_setting.action.menu_child_body)[0].tagName.toLowerCase()===j_setting.action.menu_child_body.toLowerCase()) {
				check_child = false;
				$(obj).find('>'+j_setting.action.menu_child_body).stop().slideDown().find(j_setting.action.menu_child_body).hide();
			}else if ($(obj).find(j_setting.action.menu_child_body).length<=0){
				check_child = true;
			}
		});
        

		return check_child;
	};
	function menu(j_setting){
		$(j_setting.item).each(function(idx,obj){
			$(obj).on('click',section_back);
		});
				
		switch (j_setting.action.main){
			case "menu1":
				$(j_setting.item).find(j_setting.action.menu_child_body).find(j_setting.action.menu_child_list).each(function (idx,obj){
					$(obj).attr('jtagid',$(j_setting.item).length+idx);
				}).on('click',section_back);
			break;
			default:
			break;
		}
		
		$(j_setting.item).find(j_setting.action.menu_child_body).hide();
		
		if (sessionStorage[j_setting.tag]){
			if ($(j_setting.main_body).find(j_setting.action.menu_child_list+'[jtagid="'+sessionStorage[j_setting.tag]+'"]').parentsUntil(j_setting.main_body).length>0){
				$(j_setting.main_body).find(j_setting.action.menu_child_list+'[jtagid="'+sessionStorage[j_setting.tag]+'"]').parentsUntil(j_setting.main_body).each(function(idx,obj){
					$(obj).addClass(j_setting.addclass);
					if ($(obj)[0].tagName===j_setting.action.menu_child_body.toUpperCase()) { $(obj).show(); }
				});
			}
			$('[jtagid="'+sessionStorage[j_setting.tag]+'"]').addClass(j_setting.addclass).find('>'+j_setting.action.menu_child_body).show();
		}
		
	}
	
	work(j_setting);
});
