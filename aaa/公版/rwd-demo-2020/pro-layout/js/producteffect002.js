// JavaScript Document
$(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                items: 4,
                loop: true,
                margin: 10,
				nav: true,
                loop: true,
                autoplay: true, //是否自動撥放
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
				responsive: {
                  0: {
                    items: 1
                  },
                  371: {
                    items: 2
                  },
                  701: {
                    items: 3
                  }
                }
              });
			  
              $('.play').on('click', function() {
                owl.trigger('play.owl.autoplay', [2000])
              })
              $('.stop').on('click', function() {
                owl.trigger('stop.owl.autoplay')
              })
            })