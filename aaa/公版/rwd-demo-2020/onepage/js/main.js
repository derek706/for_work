//購物清單按鈕
$(function () {
  $('#shopping-list-button').click(function () {
    $('html,body').animate({
      scrollTop: $('#cart-info').offset().top
    }, 800);
  });
});

//立即結帳按鈕
$(function () {
  $('.checkout-button').click(function () {
    $('html,body').animate({
      scrollTop: $('#billing-info').offset().top
    }, 800);
  });
});

//Go Top
$(function () {
  $("#gotop").click(function () {
    $("html,body").animate({
      scrollTop: 0
    }, 900);
    return false;
  });

  $("#checkout").click(function () {
    $('html,body').animate({
      scrollTop: $('#billing-info').offset().top
    }, 300);
    return false;
  });
});

//數量增減調整
$('#quantity-adjustment .plus').click(function () {
  var count = parseInt($('.count').val()) + 1;
  if (count > 50) count = 50;
  $('.count').val(count);
});

$('#quantity-adjustment .minus').click(function () {
  var count = parseInt($('.count').val()) - 1;
  if (count < 1) count = 1;
  $('.count').val(count);
});

//數量增減調整-2
$('#quantity-adjustment-2 .plus').click(function () {
  var count = parseInt($('.count').val()) + 1;
  if (count > 50) count = 50;
  $('.count').val(count);
});

$('#quantity-adjustment-2 .minus').click(function () {
  var count = parseInt($('.count').val()) - 1;
  if (count < 1) count = 1;
  $('.count').val(count);
});

//修改訂單按鈕
$("#shipto_type_1").click(function () {
  $("#shipToOther").slideDown("20");
});

$("#shipto_type_0").click(function () {
  $("#shipToOther").slideUp("20");
});

//發票資訊-公司
$('#invoice_type_cp').click(function () {
  $('#invoice_type_cp_info').slideDown("20");
});

$('#invoice_type_c, #invoice_type_p, #invoice_type_d, #invoice_type_pp').click(function () {
  $('#invoice_type_cp_info').slideUp("20");
});

$('#invoice_type_p').click(function () {
  $('#invoice_type_p_info').slideDown("20");
});

$('#invoice_type_c, #invoice_type_d, #invoice_type_pp, #invoice_type_cp').click(function () {
  $('#invoice_type_p_info').slideUp("20");
});
